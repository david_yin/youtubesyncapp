
var _socket = null;
var _readIntervalId = null;

function init() {
  console.log("Starting YouSync bluetooth...");

  chrome.runtime.onMessageExternal.addListener(
  function(request, sender, sendResponse) {
    if (sender.id in blacklistedIds) {
      sendResponse({"result":"sorry, could not process your message"});
      return;  // don't allow this extension access
    } else if (request.type) {
      appendLog("from "+sender.id+": "+ request.type);
      
      //Send the url via bluetooth
      
      sendResponse({"result":"Sent the url " + request.url});
    } else {
      sendResponse({"result":"Ops, I don't understand this message"});
    }
  });
  
  // Add the listener to deal with our initial connection
  chrome.bluetooth.onConnection.addListener(onConnected);

  // Check the paired Bluetooth devices then connect to the Mobile
  findAndConnect();
}

/* 
 Mobile Specific Function
*/

function findAndConnect() {
  // Get's the device list, and passes that list to the connectToMobile
  // function as a callback.
  getDeviceList(connectToMobile);
}

function onConnected(socket) {
  console.log("onConnected", socket);
  if (socket) {
    _socket = socket;
    _readIntervalId = window.setInterval(function() {
      // Reads the data from the socket and passes it to parseMobileData 
      chrome.bluetooth.read({socket: _socket}, parseMobileData);
    }, READ_INTERVAL);
  } else {
    console.error("Failed to connect.");
  }
}

function parseMobileData(data) {
  
}

function connectToMobileHelper(profiles) {
    for (var i in profiles) {
      var profile = profiles[i];
      console.log("Profiles -- ", profile.uuid);
    }
} 

function connectToMobile(deviceList) {
  console.log("connectToMobile", deviceList);
  if (deviceList !== null) {
    // Iterates through the device list looking for a device that starts with
    // Nexus as it's name then tries to connect to that device.
    for (var i in deviceList) {
      var device = deviceList[i];
      
      console.log("Found device " + device.name);
      
      if (device.name.indexOf("Nexus") === 0) {
        console.log("Connecting to Mobile", device);
        
        chrome.bluetooth.getProfiles({
          device: device
        }, connectToMobileHelper)
        
        connect(device.address, MOBILE_PROFILE);
      }
    }
  }
}


/*
 Generic BlueTooth
*/

function getDeviceList(callback) {
  console.log("Searching for bluetooth devices...");
  var deviceList = [];

  // Get the BlueTooth adapter state and verify it's ready
  chrome.bluetooth.getAdapterState(function(adapterState) {
    if (adapterState.available && adapterState.powered) {
      
      // Gets the list of paired devices and adds each one to the deviceList
      // array, when done calls the callback with the list of devices.
      chrome.bluetooth.getDevices({
        deviceCallback: function(device) { deviceList.push(device); }
      }, function () {
        console.log("Devices found", deviceList);
        if (callback) {
          callback(deviceList);
        } else {
          console.error("No callback specified.");
        }
      });
    } else {
      // If the bluetooth adapter wasn't ready or is unavailble, return null
      console.log("Bluetooth adapter not ready or unavailable.");
      callback(null);
    }
  });
}

function connect(deviceAddress, profile) {
  console.log("Connecting to device", deviceAddress, profile);
  chrome.bluetooth.connect({
    device: {address: deviceAddress},
    profile: profile
  }, function() {
    if (chrome.runtime.lastError) {
      console.error("Error on connection.", chrome.runtime.lastError.message);
    }
  });
}

init();

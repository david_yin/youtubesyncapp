/**
 * Listens for the app launching then creates the window
 *
 * @see http://developer.chrome.com/apps/app.runtime.html
 * @see http://developer.chrome.com/apps/app.window.html
 */
 var MOBILE_PROFILE = {
  uuid: '00001800-0000-1000-8000-00805f9b34fb',
  name: 'Nexus 4'
};
 
chrome.app.runtime.onLaunched.addListener(function() {
  chrome.app.window.create('index.html',
    {
      singleton: true,
    	id: "messagingEx1ID",
    	bounds: {width: 800, height: 500}
    }, function(win) {
      // Add the profile to the list of profiles we support
      chrome.bluetooth.addProfile(MOBILE_PROFILE, function(r) {
        console.log("Profile added");
      });

      // Make the profile available in the main content window.
      win.contentWindow.MOBILE_PROFILE = MOBILE_PROFILE;
    });
});

function removeProfile() {
  console.log("Removing Zephyr MOBILE_PROFILE profile");
  chrome.bluetooth.removeProfile(MOBILE_PROFILE, function(r) {
    console.log("Profile removed");
  });
}

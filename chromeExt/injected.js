chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    console.log("Content received a message");
  
    if (request.action == 'getCurTime') {
      ytplayer = window.document.getElementById("movie_player");
      intvalue = Math.floor(ytplayer.getCurrentTime());
      
      console.log("the current time is " + intvalue);
      
      sendResponse({ curTime: intvalue })
    }
    else
      sendResponse({});
  }
);

console.log("Content active");
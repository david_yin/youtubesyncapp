package com.youtubeapp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.widget.Toast;

public class MainActivity extends Activity {

	BluetoothAdapter mBluetoothAdapter;
	BluetoothDevice device;
	private BluetoothSocket btSocket = null;
	private static String address = "00:00:00:00:00:00";
	private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
	private final static int REQUEST_ENABLE_BT = 1;
	private InputStream inStream = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Uri uri = Uri.parse("http://www.youtube.com");
		Intent intent = new Intent(Intent.ACTION_VIEW, uri);
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		if (mBluetoothAdapter == null) {
		    // Device does not support Bluetooth
		}
		startActivity(intent);
	}
	protected void onStart() {
		super.onStart();
    
	  // If BT is not on, request that it be enabled.
	        // setupCommand() will then be called during onActivityResult
		String status = "";
		if (!mBluetoothAdapter.isEnabled()) {
			Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
			startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
		} else {
			String mydeviceaddress = mBluetoothAdapter.getAddress();
			String mydevicename = mBluetoothAdapter.getName();
			status = mydevicename + " : " + mydeviceaddress;
			beginConnection();
			
		}
		Toast.makeText(this, status, Toast.LENGTH_LONG).show();
		
		
	}
	
	void beginConnection() {
		device = mBluetoothAdapter.getRemoteDevice(address);
		// Two things are needed to make a connection:
		try {
			btSocket = device.createRfcommSocketToServiceRecord(MY_UUID);
		} catch (IOException e) {
			
		}
		mBluetoothAdapter.cancelDiscovery();
		
		try {
			btSocket.connect();
//			out.append("\n...Connection established and data link opened...");
		} catch (IOException e) {
			try {
				btSocket.close();
			} catch (IOException e2) {
			}
		}
		
		try {
			inStream = btSocket.getInputStream();
		} catch (IOException e) {
			
		}
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
//	private class AcceptThread extends Thread {
//	    private final BluetoothServerSocket mmServerSocket;
//	 
//	    public AcceptThread() {
//	        // Use a temporary object that is later assigned to mmServerSocket,
//	        // because mmServerSocket is final
//	        BluetoothServerSocket tmp = null;
//	        try {
//	            // MY_UUID is the app's UUID string, also used by the client code
//	            tmp = mBluetoothAdapter.listenUsingRfcommWithServiceRecord(NAME, MY_UUID);
//	        } catch (IOException e) { }
//	        mmServerSocket = tmp;
//	    }
//	 
//	    public void run() {
//	        BluetoothSocket socket = null;
//	        // Keep listening until exception occurs or a socket is returned
//	        while (true) {
//	            try {
//	                socket = mmServerSocket.accept();
//	            } catch (IOException e) {
//	                break;
//	            }
//	            // If a connection was accepted
//	            if (socket != null) {
//	                // Do work to manage the connection (in a separate thread)
//	                manageConnectedSocket(socket);
//	                try {
//						mmServerSocket.close();
//					} catch (IOException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//	                break;
//	            }
//	        }
//	    }
//	 
//	    /** Will cancel the listening socket, and cause the thread to finish */
//	    public void cancel() {
//	        try {
//	            mmServerSocket.close();
//	        } catch (IOException e) { }
//	    }
//	    
//	    public void manageConnectedSocket(BluetoothSocket socket) {
//	    	
//	    }
//	}
}

